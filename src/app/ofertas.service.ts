import { Oferta } from './shared/oferta.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http'

import 'rxjs/add/operator/toPromise'
import { URL_API } from './app.url';
@Injectable()
export class OfertasService {

  constructor(private http: Http) { }
  public getOfertas(): Promise<Array<Oferta>> {
    return this.http.get(`${URL_API.ofertas}?destaque=true`)
        .toPromise()
        .then(resposta => resposta.json())
  }
  public getOfertasPorCategoria(categoria:string):Promise<Array<Oferta>>{
    return this.http.get(`${URL_API.ofertas}?categoria=${categoria}`)
    .toPromise()
    .then((resp)=>resp.json())
  }
  public getOfertasPorId(id:number):Promise<Oferta>{
    return this.http.get(`${URL_API.ofertas}?id=${id}`)
    .toPromise()
    .then((resp)=>resp.json()[0])
  }
  public getComoUsarOfertaPorId(id:number){
    return this.http.get(`${URL_API.comoUsar}?id=${id}`)
    .toPromise()
    .then(resp=> resp.json()[0])
  }
  public getOndeFicaOfertaPorId(id:number){
    return this.http.get(`${URL_API.ondeFica}?id=${id}`)
    .toPromise()
    .then(resp=> resp.json()[0])
  }
  // public getOfertas2():Promise<Oferta[]>{
  //   return new Promise((resolve,reject)=>{
  //     resolve(this.ofertas)
  //     reject({erro:404,msg:"Not Found"})
  //   })
  //   .then((ofertas: Oferta[])=>{
  //     //épossível fazer trativa com then no proprio promise que retornar
  //     return ofertas // é possível retornar outro promise no then 
  //   })
  // }
}
