import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OfertasService } from '../ofertas.service';
import { Oferta } from '../shared/oferta.model';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.css'],
  providers: [ OfertasService ]
})
export class OfertaComponent implements OnInit {
  public oferta : Oferta 
  public control = false
  constructor(
    private routeParam : ActivatedRoute,
    private ofertasService : OfertasService
  ) { }

  ngOnInit() {
    this.ofertasService.getOfertasPorId(this.routeParam.snapshot.params['id'])
      .then((resp:Oferta )=> {
        this.oferta = resp
        this.control = true
      })
      .catch(rejected => console.log("erro ao recuperar oferta",rejected))
  }

}
