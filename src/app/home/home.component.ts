import { Oferta } from './../shared/oferta.model';
import { OfertasService } from './../ofertas.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ OfertasService ]
})
export class HomeComponent implements OnInit {
  public ofertas : Oferta[];
  constructor( private ofertasService: OfertasService ) { }

  ngOnInit() {
    // this.ofertas = this.ofertasService.getOfertas()
    this.ofertasService.getOfertas()
    .then(
      (arrOfertas:Oferta[])=>{
        this.ofertas=arrOfertas
      }
      // (rejected)=> console.log("Erro ao recuperar ofertas:",rejected.msg) // tratamento de erro pelo then (2o param)
    ).catch(rejected =>{ // tratamento de erro pelo catch
      console.log("Erro ao recuperar ofertas:",rejected.msg)
    })
  }
//teste
}
